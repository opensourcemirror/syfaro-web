---
name: LydiaPaige
link: https://twitter.com/Lydiarts

resources:
- src: syfaro.png
  params:
    icon: true
    message: Imma purple fox! :D
    source: https://twitter.com/Lydiarts/status/653415262482378752
- src: d4su3TH.png
  params:
    message: Bounce, bounce, bounce
---
