---
name: Growlbeast
link: https://twitter.com/Growlbeast

resources:
- src: syfaro_sketch_pinup.png
  params:
    nsfw: true
    source: https://twitter.com/Growlbeast/status/871995975375650817
- src: syfarobadgefull.png
  params:
    icon: true
    message: Growly face from Growlbeast is <3
    source: https://twitter.com/Growlbeast/status/799036962262892544
- src: pJI1Rpe.png
  params:
    icon: true
    message: Sly fox~
    source: https://twitter.com/growlbeast/status/603283035002273792
---
