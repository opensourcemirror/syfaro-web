---
name: wryote
link: https://twitter.com/wryote

resources:
- src: purpleblck.png
  params:
    icon: true
    message: Resist!
    source: https://twitter.com/wryote/status/829824956921954308
- src: TkqIgov9.png
  params:
    icon: true
    source: https://www.weasyl.com/~kunacoyote/submissions/827857/syfaro-icon-commission
---